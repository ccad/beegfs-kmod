#!/bin/bash
set -xeu
BEEGFS_CLIENT_RPM=`readlink -f $1`
BEEGFS_EPOCH=`rpm -qp --queryformat '%{EPOCH}' ${BEEGFS_CLIENT_RPM}`
BEEGFS_VERSION=`rpm -qp --queryformat '%{VERSION}' ${BEEGFS_CLIENT_RPM}`
BEEGFS_RELEASE=`rpm -qp --queryformat '%{RELEASE}' ${BEEGFS_CLIENT_RPM}`

# Build tarball
mkdir -p beegfs_client_module_${BEEGFS_VERSION}
cd beegfs_client_module_${BEEGFS_VERSION}
rpm2cpio ${BEEGFS_CLIENT_RPM} | cpio -idv
cd ..
mkdir -p ~/rpmbuild/SOURCES
tar cjvf ~/rpmbuild/SOURCES/beegfs-${BEEGFS_VERSION}.tar.bz2 beegfs_client_module_${BEEGFS_VERSION}/
rm -rf beegfs_client_module_${BEEGFS_VERSION}/

# Copy patches
# cp patches/*.patch ~/rpmbuild/SOURCES/

# Template spec file
sed -e "s/_BEEGFS_EPOCH_/${BEEGFS_EPOCH}/;s/_BEEGFS_VERSION_/${BEEGFS_VERSION}/;s/_BEEGFS_RELEASE_/${BEEGFS_RELEASE}/" beegfs-kmod.spec.tmpl > beegfs-kmod.spec

# Build SRPM
rpmbuild -bs --define "beegfs_epoch ${BEEGFS_EPOCH}" --define "beegfs_version ${BEEGFS_VERSION}" --define "beegfs_release ${BEEGFS_RELEASE}" beegfs-kmod.spec

# Clean
rm beegfs-kmod.spec
