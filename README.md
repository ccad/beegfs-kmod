# BeeGFS client kmod RPM creator

Builds a [BeeGFS](https://www.beegfs.io) kmod RPM package for a specific kernel version, to avoid the installed toolchain requirements that `beegfs-client` and `beegfs-client-dkms` have.

## Usage

1. Build an SRPM package using the contents of the `beegfs-client` package.

        `./build-srpm <beegfs-client-package.rpm>`

2. Build kmod package for the specified kernel version.

        `rpmbuild --rebuild --define "kversion <target kernel version>" <path/to/srpm.rpm>`

   RPM macros to customize the build:
   - `kversion`: Kernel version to build for. Required. Shouldn't include the arch suffix.
   - `kmod_name_extra`: String to append to the RPM name.
   - `build_extra_options`: Additional options to pass to the BeeGFS kernel module build.

   Example: Build with MLNX-OFED and produce a RPM named `kmod-beegfs-mofed`

        `rpmbuild --rebuild --define "kversion <target kernel version>" --define "kmod_name_extra -mofed" --define "build_extra_options OFED_INCLUDE_PATH=/usr/src/ofa_kernel/default/include" <path/to/srpm.rpm>`
